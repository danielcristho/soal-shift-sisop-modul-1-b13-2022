#! /bin/bash
echo "Enter username:\c"
read logname

time=0

while true
do
	who | grep "$logname" > /sisop/soal-shift-sisop-modul-1-b13-2022/soal1/log.txt
	if [ $? -eq 0 ]
	then
		echo "LOGIN: INFO User $logname logged in"
		if [ $time -ne 0 ]
		then
			echo "$logname was $time minutes late in logging in."
		fi
		exit
	else
		time=expr $time+1
		sleep 60
	fi
done 
